package com.db.awmd.challenge.repository;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.domain.QuickTransfer;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;

public interface AccountsRepository {

  public final Map<String, Account> accounts = new ConcurrentHashMap<>();

  void createAccount(Account account) throws DuplicateAccountIdException;

  Account getAccount(String accountId);
  
  Map<String, Account> getAllAccounts();
  

  void clearAccounts();
  public Account updateAccount(String accountId,Account account);
}
