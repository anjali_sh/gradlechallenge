package com.db.awmd.challenge.web;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.domain.QuickTransfer;
import com.db.awmd.challenge.exception.DuplicateAccountIdException;
import com.db.awmd.challenge.service.AccountsService;
import com.db.awmd.challenge.service.NotificationService;

import java.util.Map;

import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/accounts")
@Slf4j
public class AccountsController {

@Autowired
  private final NotificationService notificationService;
  private final AccountsService accountsService;
  
  @Autowired
  public AccountsController(AccountsService accountsService, NotificationService notificationService) {
    this.accountsService = accountsService;
    this.notificationService = notificationService;
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  //@RequestMapping(method = RequestMethod.POST,)
  public ResponseEntity<Object> createAccount(@RequestBody @Valid Account account) {
    log.info("Creating account {}", account);

    try {
    this.accountsService.createAccount(account);
    } catch (DuplicateAccountIdException daie) {
      return new ResponseEntity<>(daie.getMessage(), HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(HttpStatus.CREATED);
  }

  @GetMapping(path = "/{accountId}")
  public Account getAccount(@PathVariable String accountId) {
    log.info("Retrieving account for id {}", accountId);
    return this.accountsService.getAccount(accountId);
  }
  
  @GetMapping(path = "/all")
  public Map<String,Account> getAllAccounts() {
    log.info("Retrieving all the accounts");
    return this.accountsService.getAllAccounts();
  }
  
  @PostMapping(path="/transfer", consumes = MediaType.APPLICATION_JSON_VALUE)
  //@RequestMapping(method = RequestMethod.POST,)
  public ResponseEntity<Object> transferAmount(@RequestBody @Valid QuickTransfer quickTransfer ) {
    log.info("Transferring amount{} with the details", quickTransfer);

    try {
			this.accountsService.transferAmount(quickTransfer);
			this.notificationService.notifyAboutTransfer(
					this.accountsService.getAccount(quickTransfer.getAccountFromId()),
					"Amount = " + quickTransfer.getTransferAmount() + " transferred successfully to the account"
							+ quickTransfer.getAccountToId());
		} catch (Exception ex) {
      return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(HttpStatus.ACCEPTED);
  }

}
