package com.db.awmd.challenge.service;

import com.db.awmd.challenge.domain.Account;
import com.db.awmd.challenge.domain.QuickTransfer;
import com.db.awmd.challenge.repository.AccountsRepository;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountsService {

  @Getter
  private final AccountsRepository accountsRepository;
  
  @Autowired
  public AccountsService(AccountsRepository accountsRepository) {
    this.accountsRepository = accountsRepository;
  }

  public void createAccount(Account account) {
    this.accountsRepository.createAccount(account);
  }

  public Account getAccount(String accountId) {
    return this.accountsRepository.getAccount(accountId);
  }
  
  public Map<String, Account> getAllAccounts() {
	    return this.accountsRepository.getAllAccounts();
  }
  
  public void transferAmount(QuickTransfer quickTransfer) throws Exception{
	  
	  Account accountFrom = accountsRepository.getAccount(quickTransfer.getAccountFromId());
	  Account accountTo = accountsRepository.getAccount(quickTransfer.getAccountToId());
	  //condition to check accountFromId and accountToId shouldn't be the same and should exist in the repository
	  if (!quickTransfer.getAccountFromId().equals(quickTransfer.getAccountToId()) && null!= accountFrom && null!= accountTo) {
		 
		  BigDecimal accountFromBal = BigDecimal.valueOf (accountFrom.getBalance().doubleValue() - quickTransfer.getTransferAmount().doubleValue());
		  //if account bal is less than zero i.e. negative throw an exception
		  if (accountFromBal.intValue() < 0) {
			  throw new Exception("Insufficient funds to transfer");
		  }
		  accountFrom.setBalance(BigDecimal.valueOf (accountFrom.getBalance().doubleValue() - quickTransfer.getTransferAmount().doubleValue()));
		  accountTo.setBalance(BigDecimal.valueOf (accountTo.getBalance().doubleValue() + quickTransfer.getTransferAmount().doubleValue()));
		  //call updateAccount to save the updated balance in the repository
		  accountsRepository.updateAccount(quickTransfer.getAccountFromId(), accountFrom);
		  accountsRepository.updateAccount(quickTransfer.getAccountToId(), accountTo);
	  }
	  else {
		  throw new Exception("Could not perform the transfer");
	  }
	  }
}
